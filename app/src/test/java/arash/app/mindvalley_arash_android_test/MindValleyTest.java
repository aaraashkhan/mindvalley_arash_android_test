package arash.app.mindvalley_arash_android_test;

import android.content.Context;
import android.test.InstrumentationTestCase;
import android.test.mock.MockContext;
import android.util.Log;

import org.junit.Test;

import arash.app.mindvalley_arash_android_test.Utils.Utils;

/**
 * Created by arash on 8/27/16.
 */
public class MindValleyTest extends InstrumentationTestCase {

    Context context;

    public void setUp() throws Exception {
        super.setUp();

        context = new MockContext();

        assertNotNull(context);

    }

    @Test
    public void testURLCreation() {
        context = new MockContext();
        String actual = Utils.getContentImageUrl(context,"mindvalley");
        // expected value is https://images.unsplash.com/photo-mindvalley
        String expected = "wrong variable!";
        assertNull(actual);
        assertNotSame(actual,expected);
    }
}
