package arash.app.mindvalley_arash_android_test.Utils;

import android.content.Context;

import arash.app.mindvalley_arash_android_test.R;

/**
 * Created by arash on 8/24/16.
 */
public class Utils {
    public static String getContentImageUrl(Context context, String leadingValue) {
        String baseUrl = context.getString(R.string.images_base_url);

        return baseUrl + leadingValue;
    }
}
