package arash.app.mindvalley_arash_android_test.RequestManager.Entity;

/**
 * Created by arash on 8/24/16.
 */
public class LinksData {

    private String self;
    private String html;
    private String photos;
    private String likes;

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public String getPhotos() {
        return photos;
    }

    public void setPhotos(String photos) {
        this.photos = photos;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }
}
