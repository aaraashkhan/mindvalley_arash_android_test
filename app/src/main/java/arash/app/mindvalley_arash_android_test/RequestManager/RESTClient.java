package arash.app.mindvalley_arash_android_test.RequestManager;

import android.content.Context;
import android.os.Looper;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;

import java.lang.ref.WeakReference;

import arash.app.mindvalley_arash_android_test.R;

/**
 * Created by arash on 8/24/16.
 */
public class RESTClient {
    private WeakReference<Context> context;
    private AsyncHttpClient client;
    public static AsyncHttpClient syncHttpClient= new SyncHttpClient();
    public static AsyncHttpClient asyncHttpClient = new AsyncHttpClient();

    public RESTClient(Context context) {
        this.context = new WeakReference<>(context.getApplicationContext());

        client = getClient();
    }

    public void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.get(getAbsoluteUrl(url), params, getResponseHandler("get", url, params, responseHandler));
    }

    public void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.post(getAbsoluteUrl(url), params, getResponseHandler("post", url, params, responseHandler));
    }

    private String getAbsoluteUrl(String relativeUrl) {
        return context.get().getString(R.string.api_url) + relativeUrl;
    }

    private AsyncHttpResponseHandler getResponseHandler(
            final String method, final String url, final Object params,
            final AsyncHttpResponseHandler responseHandler) {
        return new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
                responseHandler.onStart();
            }

            @Override
            public void onSuccess(int statusCode, org.apache.http.Header[] headers, byte[] responseBody) {
                responseHandler.onSuccess(statusCode, headers, responseBody);
            }

            @Override
            public void onFailure(int statusCode, org.apache.http.Header[] headers, byte[] responseBody, Throwable error) {
                responseHandler.onFailure(statusCode, headers, responseBody, error);
            }
        };
    }


    private AsyncHttpClient getClient() {
        // Return the synchronous HTTP client when the thread is not prepared
        if (Looper.myLooper() == null) {
            syncHttpClient.addHeader("Accept", "*/*");
            return syncHttpClient;
        }

        asyncHttpClient.addHeader("Accept", "*/*");
        return asyncHttpClient;
    }
}
