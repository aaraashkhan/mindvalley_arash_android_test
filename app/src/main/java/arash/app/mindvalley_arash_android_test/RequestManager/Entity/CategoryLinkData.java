package arash.app.mindvalley_arash_android_test.RequestManager.Entity;

/**
 * Created by arash on 8/24/16.
 */
public class CategoryLinkData {
    private String self;
    private String photos;

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public String getPhotos() {
        return photos;
    }

    public void setPhotos(String photos) {
        this.photos = photos;
    }


}
