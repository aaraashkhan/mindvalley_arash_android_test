package arash.app.mindvalley_arash_android_test.RequestManager;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import arash.app.mindvalley_arash_android_test.RequestManager.Cache.CacheData;
import arash.app.mindvalley_arash_android_test.R;

/**
 * Created by arash on 8/24/16.
 */
public class ImageManager {

    public void fetchAndSaveImage(Context mContext, final ImageView imageView, final String url){

        if (CacheData.getInstance().getImagesCache().get(url) != null){
            imageView.setImageDrawable(CacheData.getInstance().getImagesCache().get(url));
        }else {
            Picasso.with(mContext)
                    .load(url)
                    .fit()
                    .placeholder(R.drawable.wait)
                    .error(R.drawable.error)
                    .into(imageView, new Callback() {
                        @Override
                        public void onSuccess() {
                            BitmapDrawable bitmapDrawable = ((BitmapDrawable) imageView.getDrawable());
                            CacheData.getInstance().addToImageToCache(url, bitmapDrawable );
                        }

                        @Override
                        public void onError() {

                        }
                    });
        }
    }
}
