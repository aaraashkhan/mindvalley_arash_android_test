package arash.app.mindvalley_arash_android_test.RequestManager.Entity;

/**
 * Created by arash on 8/24/16.
 */
public class UserData {
    private String id;
    private String username;
    private ProfileImageData profile_image;
    private LinksData links;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LinksData getLinks() {
        return links;
    }

    public void setLinks(LinksData links) {
        this.links = links;
    }

    public ProfileImageData getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(ProfileImageData profile_image) {
        this.profile_image = profile_image;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
