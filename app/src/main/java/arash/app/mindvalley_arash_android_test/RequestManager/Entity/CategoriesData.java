package arash.app.mindvalley_arash_android_test.RequestManager.Entity;

/**
 * Created by arash on 8/24/16.
 */
public class CategoriesData {
    private String id;
    private String title;
    private int photo_count;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getPhoto_count() {
        return photo_count;
    }

    public void setPhoto_count(int photo_count) {
        this.photo_count = photo_count;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
