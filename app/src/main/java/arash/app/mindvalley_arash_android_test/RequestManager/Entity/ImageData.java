package arash.app.mindvalley_arash_android_test.RequestManager.Entity;

import java.util.ArrayList;

/**
 * Created by arash on 8/24/16.
 */
public class ImageData {
    private String id;
    private int width;
    private int height;
    private String color;
    private int likes;
    private boolean liked_by_user;
    private UserData user;
    private URLData urls;
    private LinksData links;
    private ArrayList<CategoriesData> categories;

    public ArrayList<CategoriesData> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<CategoriesData> categories) {
        this.categories = categories;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isLiked_by_user() {
        return liked_by_user;
    }

    public void setLiked_by_user(boolean liked_by_user) {
        this.liked_by_user = liked_by_user;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public LinksData getLinks() {
        return links;
    }

    public void setLinks(LinksData links) {
        this.links = links;
    }

    public URLData getUrls() {
        return urls;
    }

    public void setUrls(URLData urls) {
        this.urls = urls;
    }

    public UserData getUser() {
        return user;
    }

    public void setUser(UserData user) {
        this.user = user;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }
}
