package arash.app.mindvalley_arash_android_test.RequestManager.Cache;

import android.graphics.drawable.Drawable;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by arash on 8/25/16.
2 */

@Module
public class CacheData {
    private static CacheData ourInstance = new CacheData();
    private static int IMAGE_HEAP = 10;
    private static int DOCUMENT_HEAP = 10;

    public static int getImageHeap() {
        return IMAGE_HEAP;
    }

    public static void setImageHeap(int imageHeap) {
        IMAGE_HEAP = imageHeap;
    }

    public static int getDocumentHeap() {
        return DOCUMENT_HEAP;
    }

    public static void setDocumentHeap(int documentHeap) {
        DOCUMENT_HEAP = documentHeap;
    }

    private CacheData(){

    }

    @Provides
    @Singleton
    public static CacheData getInstance() {
        return ourInstance;
    }
    private HashMap<String, Drawable> imagesCache;
    private HashMap<String, String> documentsCache;


    public HashMap<String, String> getDocumentsCache() {
        if (documentsCache == null){
            documentsCache = new HashMap<>();
        }
        return documentsCache;
    }

    public void setDocumentsCache(HashMap<String, String> documentsCache) {
        this.documentsCache = documentsCache;
    }

    public HashMap<String, Drawable> getImagesCache() {
        if (imagesCache == null){
            imagesCache = new HashMap<>();
        }
        return imagesCache;
    }

    public void setImagesCache(HashMap<String, Drawable> imagesCache) {
        this.imagesCache = imagesCache;
    }

    public void addToImageToCache(String id,Drawable drawable){
        if (imagesCache == null){
            imagesCache = new HashMap<>();
        }

        if (imagesCache.size() >= IMAGE_HEAP){//HEAP management! If heap grows over 5 items they get replaced
            Iterator it = imagesCache.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry)it.next();
                it.remove(); // avoids a ConcurrentModificationException
                imagesCache.remove(pair.getKey());
                break;
            }
            imagesCache.put(id, drawable);
        }else {
            imagesCache.put(id, drawable);
        }
    }

    public void addToDocumentsToCache(String id, String value){
        if (documentsCache == null){
            documentsCache = new HashMap<>();
        }

        if (documentsCache.size() >= DOCUMENT_HEAP){//HEAP management! If heap grows over 5 items they get replaced
            Iterator it = documentsCache.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry)it.next();
                it.remove(); // avoids a ConcurrentModificationException
                documentsCache.remove(pair.getKey());
                break;
            }
            documentsCache.put(id,value);
        }else {
            documentsCache.put(id,value);
        }

    }

    public void flushCacheData(){
        documentsCache = new HashMap<>();
        imagesCache = new HashMap<>();
    }
}
