package arash.app.mindvalley_arash_android_test.RequestManager;

import android.content.Context;
import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONObject;

import rx.Observer;

/**
 * Created by arash on 8/24/16.
 */
public class GeneralContentManager {
    public enum Conent {
        IMAGE,
        JSON_ARRAY,
        JSON_OBJECT,
        RAW_STRING
    }

    private Context mContext;

    public GeneralContentManager(Context context, ImageView imageView, String url){
        mContext = context;
        new ImageManager().fetchAndSaveImage(mContext,imageView,url);
    }

    public GeneralContentManager(Context context, String url, Conent contetType){
        mContext = context;
        switch (contetType){
            case JSON_ARRAY:
                fetchJSONArray(mContext,url);
                break;
            case JSON_OBJECT:
                fetchJSONObject(mContext,url);
                break;
            case  RAW_STRING:
                fetchRawString(mContext,url);
                break;
        }
    }

    private void fetchJSONArray(Context context, String url){
        new DocumentManager().fetchAndSaveDocumentsJSONArray(context, url, new Observer<JSONArray>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(JSONArray jsonArray) {

            }
        });
    }

    private void fetchJSONObject(Context context, String url){
        new DocumentManager().fetchAndSaveDocumentsJSONObject(context, url, new Observer<JSONObject>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(JSONObject jsonObject) {

            }
        });
    }

    private void fetchRawString(Context context, String url){
        new DocumentManager().fetchAndSaveDocumentsRaw(context, url, new Observer<String>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(String rawString) {

            }
        });
    }
}
