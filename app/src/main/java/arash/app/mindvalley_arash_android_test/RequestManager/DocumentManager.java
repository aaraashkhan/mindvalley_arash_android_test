package arash.app.mindvalley_arash_android_test.RequestManager;

import android.content.Context;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import arash.app.mindvalley_arash_android_test.RequestManager.Cache.CacheData;
import rx.Observer;

/**
 * Created by arash on 8/25/16.
 */
public class DocumentManager {

    public void fetchAndSaveDocumentsJSONArray(final Context mContext, final String url, final Observer<JSONArray> observer){

        if (CacheData.getInstance().getDocumentsCache().get(url) != null){//Check with cache and if exist return cache
            try {
                rx.Observable.just(new JSONArray(CacheData.getInstance().getDocumentsCache().get(url))).subscribe(observer);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            RESTClient restClient = new RESTClient(mContext);
            restClient.get(url, new RequestParams(), new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    CacheData.getInstance().addToDocumentsToCache(url,new String(responseBody));
                    try {
                        rx.Observable.just(new JSONArray(new String(responseBody))).subscribe(observer);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    Toast.makeText(mContext,"There appears to be a problem connecting to server!",Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    public void fetchAndSaveDocumentsJSONObject(final Context mContext, final String url, final Observer<JSONObject> observer){

        if (CacheData.getInstance().getDocumentsCache().get(url) != null){//Check with cache and if exist return cache
            try {
                rx.Observable.just(new JSONObject(CacheData.getInstance().getDocumentsCache().get(url))).subscribe(observer);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            RESTClient restClient = new RESTClient(mContext);
            restClient.get(url, new RequestParams(), new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    CacheData.getInstance().addToDocumentsToCache(url, new String(responseBody));
                    try {
                        rx.Observable.just(new JSONObject(new String(responseBody))).subscribe(observer);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    Toast.makeText(mContext, "There appears to be a problem connecting to server!", Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    public void fetchAndSaveDocumentsRaw(final Context mContext, final String url, final Observer<String> observer){

        if (CacheData.getInstance().getDocumentsCache().get(url) != null){//Check with cache and if exist return cache
            rx.Observable.just(CacheData.getInstance().getDocumentsCache().get(url)).subscribe(observer);
        }else {
            RESTClient restClient = new RESTClient(mContext);
            restClient.get(url, new RequestParams(), new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    CacheData.getInstance().addToDocumentsToCache(url, new String(responseBody));
                    rx.Observable.just(new String(responseBody)).subscribe(observer);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    Toast.makeText(mContext, "There appears to be a problem connecting to server!", Toast.LENGTH_LONG).show();
                }
            });
        }
    }
}
