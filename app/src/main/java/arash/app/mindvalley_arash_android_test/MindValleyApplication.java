package arash.app.mindvalley_arash_android_test;

import android.app.Application;

import arash.app.mindvalley_arash_android_test.UI.HomeFragment;

/**
 * Created by arash on 8/25/16.
 */
public class MindValleyApplication extends Application {
    public static MainActivity mainActivity;
    public static HomeFragment homeFragment;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public static HomeFragment getHomeFragment() {
        return homeFragment;
    }

    public static MainActivity getMainActivity() {
        return mainActivity;
    }
}
