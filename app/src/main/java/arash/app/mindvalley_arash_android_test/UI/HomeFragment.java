package arash.app.mindvalley_arash_android_test.UI;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;

import java.util.ArrayList;
import java.util.Arrays;

import arash.app.mindvalley_arash_android_test.MindValleyApplication;
import arash.app.mindvalley_arash_android_test.UI.Adapters.*;
import arash.app.mindvalley_arash_android_test.RequestManager.Entity.*;
import arash.app.mindvalley_arash_android_test.R;
import arash.app.mindvalley_arash_android_test.RequestManager.RESTClient;
import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;

/**
 * A placeholder fragment containing a simple view.
 */
public class HomeFragment extends Fragment {

    private static final long ANIMATION_DURATION = 1500;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private PicturesTimeLineAdapter picturesAdapter;
    private EmptyViewAdapter emptyViewAdapter;
    private ArrayList<ImageData> imagesData;

    public HomeFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_main, container, false);
        MindValleyApplication.homeFragment = this;
        // Swipe Refresh Layout
        mSwipeRefreshLayout = (SwipeRefreshLayout)fragmentView.findViewById(R.id.swipe_layout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(true);
                fetchAPIData();
            }
        });

        // RecyclerView
        recyclerView = (RecyclerView) fragmentView.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new SlideInLeftAnimator());
        recyclerView.getItemAnimator().setAddDuration(ANIMATION_DURATION);
        recyclerView.getItemAnimator().setRemoveDuration(ANIMATION_DURATION);
        recyclerView.getItemAnimator().setMoveDuration(ANIMATION_DURATION);
        recyclerView.getItemAnimator().setChangeDuration(ANIMATION_DURATION);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener(){
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int topRowVerticalPosition =
                        (recyclerView == null || recyclerView.getChildCount() == 0) ? 0 : recyclerView.getChildAt(0).getTop();
                mSwipeRefreshLayout.setEnabled(topRowVerticalPosition >= 0);

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        emptyViewAdapter = new EmptyViewAdapter();
        recyclerView.setAdapter(emptyViewAdapter);

        fetchAPIData();
        return fragmentView;
    }

    private void setAdapter(){
        picturesAdapter = new PicturesTimeLineAdapter(imagesData,
                new PicturesTimeLineAdapter.PictureViewHolder.ClickListener() {
                    @Override
                    public void onClick(View v, int position) {

                    }
                });
        recyclerView.setAdapter(picturesAdapter);

    }

    public void fetchAPIData(){
        mSwipeRefreshLayout.setRefreshing(true);
        RESTClient restClient = new RESTClient(getContext());
        restClient.get("raw/wgkJgazE", new RequestParams(), new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                imagesData = new ArrayList<>(Arrays.asList(new GsonBuilder()
                        .create().fromJson(new String(responseBody), ImageData[].class)));
                setAdapter();
                mSwipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toast.makeText(getContext(),"There appears to be a problem connecting to server!",Toast.LENGTH_LONG).show();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        recyclerView.setAdapter(null); // will trigger the recycling in the adapter
    }
}
