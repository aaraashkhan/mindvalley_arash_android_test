package arash.app.mindvalley_arash_android_test.UI.Adapters;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import arash.app.mindvalley_arash_android_test.R;

/**
 * Created by arash on 8/24/16.
 */
public class EmptyViewAdapter extends RecyclerView.Adapter<EmptyViewAdapter.EmptyViewHolder>{

    private Context mContext;

    public EmptyViewAdapter() {
    }

    @Override
    public EmptyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view = LayoutInflater.from(mContext).inflate(R.layout.empty_view_item, parent, false);

        EmptyViewHolder emptyViewHolder = new EmptyViewHolder(view);
        return emptyViewHolder;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(final EmptyViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public static class EmptyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ProgressBar progressBar;

        private ClickListener clickListener;

        public EmptyViewHolder(View itemView) {
            super(itemView);

            progressBar = (ProgressBar) itemView.findViewById(R.id.pbHeaderProgress);

            itemView.setOnClickListener(this);
        }

        /* Interface for handling clicks - both normal and long ones. */
        public interface ClickListener {
            public void onClick(View v, int position);
        }

        /* Setter for itemClickListener. */
        public void setOnClickListener(ClickListener clickListener) {
            this.clickListener = clickListener;
        }

        @Override
        public void onClick(View v) {
            clickListener.onClick(v, getAdapterPosition());
        }

    }
}
