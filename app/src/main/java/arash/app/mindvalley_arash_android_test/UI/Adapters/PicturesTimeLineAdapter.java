package arash.app.mindvalley_arash_android_test.UI.Adapters;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import arash.app.mindvalley_arash_android_test.RequestManager.Entity.*;
import arash.app.mindvalley_arash_android_test.R;
import arash.app.mindvalley_arash_android_test.RequestManager.GeneralContentManager;

/**
 * Created by arash on 8/24/16.
 */
public class PicturesTimeLineAdapter extends RecyclerView.Adapter<PicturesTimeLineAdapter.PictureViewHolder>{

    ArrayList<ImageData> mDataArray;
    private PictureViewHolder.ClickListener itemClickListener;
    private Context mContext;

    public PicturesTimeLineAdapter(ArrayList<ImageData> jsonArray, PictureViewHolder.ClickListener listener) {
        mDataArray = jsonArray;
        this.itemClickListener = listener;
    }

    @Override
    public PictureViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view = LayoutInflater.from(mContext).inflate(R.layout.picture_item, parent, false);

        PictureViewHolder pictureViewHolder = new PictureViewHolder(view);
        return pictureViewHolder;
    }

    @Override
    public void onViewRecycled(PictureViewHolder holder) {
        super.onViewRecycled(holder);
        holder.cancelLoading();
    }



    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(final PictureViewHolder holder, int position) {
// Get the current album's data in JSON form
        final ImageData imageData = mDataArray.get(position);

        // Requesting content manager for image (from cache or from url)
        new GeneralContentManager(mContext,
                holder.pictureImageView,imageData.getUrls().getRegular());

        new GeneralContentManager(mContext,"raw/wgkJgazE", GeneralContentManager.Conent.RAW_STRING);

        holder.artistTextView.setText(imageData.getUser().getUsername());
        holder.likesTextView.setText(String.valueOf(imageData.getLikes()));
        if (imageData.isLiked_by_user()){
            holder.likeImageView.setImageDrawable(mContext.getDrawable(R.drawable.like_full));
        }else {
            holder.likeImageView.setImageDrawable(mContext.getDrawable(R.drawable.like_empty));
        }

        holder.setOnClickListener(new PictureViewHolder.ClickListener() {
            @Override
            public void onClick(View v, int pos) {
                itemClickListener.onClick(v, pos);
            }
        });
        holder.likeImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (imageData.isLiked_by_user()){
                    imageData.setLiked_by_user(false);
                    holder.likeImageView.setImageDrawable(mContext.getDrawable(R.drawable.like_empty));
                }else {
                    imageData.setLiked_by_user(true);
                    holder.likeImageView.setImageDrawable(mContext.getDrawable(R.drawable.like_full));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataArray.size();
    }

    public static class PictureViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView pictureImageView;
        TextView artistTextView;
        TextView likesTextView;
        ImageView likeImageView;

        private ClickListener clickListener;

        public PictureViewHolder(View itemView) {
            super(itemView);

            pictureImageView = (ImageView) itemView.findViewById(R.id.mainPictureImageView);
            artistTextView = (TextView) itemView.findViewById(R.id.artistTextView);
            likesTextView = (TextView) itemView.findViewById(R.id.likesTextView);
            likeImageView = (ImageView) itemView.findViewById(R.id.likeImageView);

            itemView.setOnClickListener(this);
        }

        /* Interface for handling clicks - both normal and long ones. */
        public interface ClickListener {
            public void onClick(View v, int position);
        }

        /* Setter for itemClickListener. */
        public void setOnClickListener(ClickListener clickListener) {
            this.clickListener = clickListener;
        }

        @Override
        public void onClick(View v) {
            clickListener.onClick(v, getAdapterPosition());
        }

        //This method cancels image loading!
        public void cancelLoading() {
            Picasso.with(pictureImageView.getContext())
                    .cancelRequest(pictureImageView);
            pictureImageView.setImageDrawable(null);
        }
    }
}
